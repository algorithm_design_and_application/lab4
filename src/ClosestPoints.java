import java.util.Scanner;

public class ClosestPoints {
    public static double calculateDistance(double x1, double y1, double x2, double y2) {
        return Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int Q = scanner.nextInt();

        for (int q = 0; q < Q; ++q) {
            int N = scanner.nextInt();

            double[][] points = new double[N][2];

            for (int i = 0; i < N; ++i) {
                points[i][0] = scanner.nextDouble();
                points[i][1] = scanner.nextDouble();
            }

            double minDistance = Double.MAX_VALUE;

            for (int i = 0; i < N - 1; ++i) {
                for (int j = i + 1; j < N; ++j) {
                    double distance = calculateDistance(points[i][0], points[i][1], points[j][0], points[j][1]);
                    minDistance = Math.min(minDistance, distance);
                }
            }

            if (minDistance <= 10000.0) {
                System.out.printf("%.1f\n", minDistance);
            } else {
                System.out.println("No answer");
            }
        }

        scanner.close();
    }
}