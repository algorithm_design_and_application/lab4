import java.util.Scanner;

public class ModuloCalculation {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int Q = scanner.nextInt();

        for (int q = 0; q < Q; ++q) {
            int a = scanner.nextInt();
            int b = scanner.nextInt();
            int c = scanner.nextInt();

            // คำนวณ a^b mod c
            long result = powerMod(a, b, c);

            System.out.println(result);
        }

        scanner.close();
    }

    // ฟังก์ชันที่ใช้คำนวณ a^b mod c โดยใช้วิธีการแยกเลขยกกำลัง
    private static long powerMod(int a, int b, int c) {
        long result = 1;
        a = a % c;

        while (b > 0) {
            if (b % 2 == 1) {
                result = (result * a) % c;
            }

            b = b >> 1;
            a = (int) ((long) a * a % c);
        }

        return result;
    }
}